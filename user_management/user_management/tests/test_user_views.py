import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from user.models import User
from user.api.v1.serializers import UserSerializer

client = Client()

class SingleUserGetTest(TestCase):
    def setUp(self):
        self.himanshu = User.objects.create(
            first_name = "Himanshu",
            last_name = "Das",
            company_name = "Himanshu Das",
            city = "Kolkata",
            state = "West Bengal",
            zip_code = 125433,
            email = "himanshu.das@gmail.com",
            web = "www.himanshudas.com",
            age = 25,
        )

    def test_single_user_get(self):
        response = client.get(reverse('users', kwargs={'pk': self.himanshu.pk}))
        user = User.objects.get(pk=self.himanshu.pk)
        serializer = UserSerializer(user)
        self.assertEqual(response.data['data'], serializer.data)
        self.assertEqual(response.data['code'], status.HTTP_200_OK)


class AllUserGetTest(TestCase):
    def setUp(self):
        User.objects.create(
            first_name = "Himanshu",
            last_name = "Das",
            company_name = "Himanshu Das",
            city = "Kolkata",
            state = "West Bengal",
            zip_code = 125433,
            email = "himanshu.das@gmail.com",
            web = "www.himanshudas.com",
            age = 25,
            )
        User.objects.create(
            first_name = "Akhand",
            last_name = "Prataph",
            company_name = "Akhand Prataph",
            city = "Kanpur",
            state = "Uter Pradesh",
            zip_code = 123322,
            email = "akhand.prataph@gmail.com",
            web = "www.akhand.prataph.com",
            age = 26,
            )

    def test_all_user_get(self):
        response = client.get(reverse('users'))
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        self.assertEqual(response.data['data'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        

class UserPutTest(TestCase):
    def setUp(self):
        self.himanshu = User.objects.create(
            first_name = "Himanshu",
            last_name = "Das",
            company_name = "Himanshu Das",
            city = "Kolkata",
            state = "West Bengal",
            zip_code = 125433,
            email = "himanshu.das@gmail.com",
            web = "www.himanshudas.com",
            age = 25,
        )
        self.update_himanshu = {
            "first_name": "Hima",
            "last_name": "Daas"
        }

    def test_user_put(self):
        response = client.put(
            reverse('users', 
            kwargs={'pk': self.himanshu.pk}), 
            data=json.dumps(self.update_himanshu), 
            content_type='application/json'
            )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserPostTest(TestCase):
    def setUp(self):
       self.himanshu = {
            "first_name": "Himanshu",
            "last_name": "Das",
            "company_name": "Himanshu Das",
            "city": "Kolkata",
            "state": "West Bengal",
            "zip_code": 125433,
            "email": "himanshu.das@gmail.com",
            "web": "www.himanshudas.com",
            "age": 25,
        }

    def test_user_post(self):
        response = client.post(reverse('users'), data=json.dumps(self.himanshu), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserDeleteTest(TestCase):
    def setUp(self):
        self.himanshu = User.objects.create(
            first_name = "Himanshu",
            last_name = "Das",
            company_name = "Himanshu Das",
            city = "Kolkata",
            state = "West Bengal",
            zip_code = 125433,
            email = "himanshu.das@gmail.com",
            web = "www.himanshudas.com",
            age = 25,
        )

    def test_user_delete(self):
        response = client.delete(
            reverse('users', 
            kwargs={'pk': self.himanshu.pk}
            ))
        self.assertEqual(response.status_code, status.HTTP_200_OK) 

