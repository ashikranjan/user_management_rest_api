from django.test import TestCase
from user.models import User

class UserTest(TestCase):
	def setUp(self):
		User.objects.create(
			first_name = "Himanshu",
			last_name = "Das",
			company_name = "Himanshu Das",
			city = "Kolkata",
			state = "West Bengal",
			zip_code = 125433,
			email = "himanshu.das@gmail.com",
			web = "www.himanshudas.com",
			age = 25,
			)
		User.objects.create(
			first_name = "Akhand",
			last_name = "Prataph",
			company_name = "Akhand Prataph",
			city = "Kanpur",
			state = "Uter Pradesh",
			zip_code = 123322,
			email = "akhand.prataph@gmail.com",
			web = "www.akhand.prataph.com",
			age = 26,
			)

	def test_user(self):
		user_himanshu = User.objects.get(first_name="Himanshu")
		user_akhand = User.objects.get(first_name="Akhand")
		self.assertEqual(user_himanshu.get_state(), "Himanshu belongs to West Bengal")
		self.assertEqual(user_akhand.get_state(), "Akhand belongs to Uter Pradesh")