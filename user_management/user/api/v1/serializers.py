from rest_framework import serializers
from user.models import User

class UserSerializer(serializers.Serializer):
	id = serializers.IntegerField(required=False)
	first_name = serializers.CharField(max_length=20, required=True)
	last_name = serializers.CharField(max_length=20, required=True)
	company_name = serializers.CharField(max_length=100, required=False)
	city = serializers.CharField(max_length=20, required=True)
	state = serializers.CharField(max_length=20, required=True)
	zip_code = serializers.IntegerField(required=True)
	email = serializers.EmailField(max_length=70, required=True)
	web = serializers.CharField(max_length=100, required=False, allow_blank=True)
	age = serializers.IntegerField(required=True)

	class Meta:
		model = User
		fields = '__all__'

	def create(self, validated_data):
		return User.objects.create(**validated_data)