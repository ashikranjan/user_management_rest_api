from django.db.models import Q
from base_services.api.v1.services.base import APIBaseService
from user.api.v1.serializers import UserSerializer
from base_services.api.v1.services.pagination import PaginationService
from user.models import User

class UserListServices(APIBaseService):
    def __init__(self, request):
        self.request = request

    @staticmethod
    def get_sorted_data(sorting_params, filtered_users):
        sorted_users = filtered_users
        if sorting_params:
            try:
                sorted_users = filtered_users.order_by(sorting_params)
            except Exception as e:
                sorted_users = filtered_users

        return sorted_users

    @staticmethod
    def get_filtered_user(name):
        users = User.objects.all()
        if name:
            users = users.filter(Q(last_name__icontains=name) | Q(first_name__icontains=name))

        return users

    def list_user(self):
        query_params = self.request.query_params
        page_number = query_params.get('page', 1)
        page_size = query_params.get('limit', 5)
        name = query_params.get('name', None)
        sorting_params = query_params.get('sort', None)
        filtered_users = self.get_filtered_user(name)
        users = self.get_sorted_data(sorting_params, filtered_users)
        pagination_service = PaginationService(page_size, page_number)      
        paginated_response = pagination_service.get_paginated_data(users)
        data = paginated_response.get('data')
        count = paginated_response.get('count')
        if not data:
            response = self.get_404_response(errors="User not available")
            response['count'] = count
            return response

        serializer = UserSerializer(data, many=True)
        response = self.set_response(data=serializer.data)
        response['count'] = count
        return response

    def create_user(self):
        serializer = UserSerializer(data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            response = self.record_created(data=serializer.data)
            return response

        response = self.pre_condition_failed(errors="Either no data or invalid data provided")
        return response


class ManageUserServices(APIBaseService):
    def __init__(self, request, pk):
        self.request = request
        self.pk = pk

    def get_user(self):
        try:
            user = User.objects.get(pk=self.pk)
        except Exception as e:
            return self.get_404_response(errors="No user found")

        serializer = UserSerializer(user)
        response = self.set_response(data=serializer.data)
        return response

    def update_or_create_user(self):
        try:
            user = User.objects.get(pk=self.pk)
        except Exception as e:
            return self.get_404_response(errors="No user found")

        serializer = UserSerializer(user, data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            response = self.set_response(serializer.data)
            return response

        response = self.get_baked_400_response(errors=serializer.errors)
        return response

    def delete_user(self):
        try:
            user = User.objects.get(pk=self.pk)
        except Exception as e:
            return self.get_404_response(errors="No user found")

        user.delete()
        response = self.action_performed_no_content()
        return response
