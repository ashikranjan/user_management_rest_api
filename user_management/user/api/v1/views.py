from rest_framework.views import APIView
from rest_framework.response import Response
from user.api.v1.services.manage_user import (
    UserListServices,
    ManageUserServices
    ) 

class UserList(APIView):
    """
    List all the users, or create a new user user based on data provided
    """

    def get(self, request):
        services = UserListServices(self.request)
        response = services.list_user()
        return Response(response)

    def post(self, request):
        services = UserListServices(self.request)
        response = services.create_user()
        return Response(response)


class ManageUser(APIView):
    """Get user filter by their id"""
    def get(self, request, pk):
        service = ManageUserServices(request, pk)
        response = service.get_user()
        return Response(response)

    def put(self, request, pk):
        service = ManageUserServices(request, pk)
        response = service.update_or_create_user()
        return Response(response)

    def delete(self, request, pk):
        service = ManageUserServices(request, pk)
        response = service.delete_user()
        return Response(response)
