from django.db import models

class User(models.Model):
	first_name = models.CharField(max_length=20, null=False, blank=False)
	last_name = models.CharField(max_length=20, null=False, blank=False)
	company_name = models.CharField(max_length=100, null=True, blank=True)
	city = models.CharField(max_length=20, null=False, blank=False)
	state = models.CharField(max_length=20, null=False, blank=False)
	zip_code = models.IntegerField(null=False, blank=False)
	email = models.EmailField(max_length=70, null=False, blank=False, unique=True)
	web = models.CharField(max_length=100, null=True, blank=True)
	age = models.IntegerField(null=False, blank=False)

	def get_state(self):
		return self.first_name + " belongs to " + self.state 

	def __str__(self):
		return "{} {}".format(self.first_name, self.last_name)