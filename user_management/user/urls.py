from django.conf.urls import url
from user.api.v1 import views

urlpatterns = [
    url(r'^$', views.UserList.as_view(), name='users'),
    url(r'^(?P<pk>\d+)/$', views.ManageUser.as_view(), name='users')
]
