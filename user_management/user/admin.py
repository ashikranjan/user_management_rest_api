from django.contrib import admin
from user.models import User

class UserAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'company_name', 'city', 'state', 'zip_code', 'email', 'web', 'age')


admin.site.register(User, UserAdmin)

