Setup guide for linux | python3
=====================================


## Install Packages
-  sudo apt-get install python3 python-dev python3-dev build-essential 
   libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev python3-pip


## Install git if not installed
-  sudo apt-get install git


## Clone the project from gitlab
-  git clone https://gitlab.com/ashikranjan/user_management_rest_api.git


## Create Virtual Environment
-  virtualenv -p python3 my_venv


## Activate Virtual Environment
-  source my_venv/bin/activate


## Install project requirement
-  pip install -r requirement.text


## Run python server and make sure running
-  python manage.py runserver





Database(MYSQL) connectivity with project
==========================================

## Install Mysql server
-  sudo apt-get instal mysql_secure_installation

## Go to mysql terminal
-  mysql

## Create Database
-  CREATE DATABASE datapeace_db

## Create Local User and grant privilages on database
-  GRANT ALL ON datapeace_db.* TO 'datapeace'@'localhost' IDENTIFIED BY 'datapeace@123';


## Come out of mysql terminal and make the migration changes
-  python manage.py migrate

## Create superuser using valid user name and password so that django admin can be used
-  python manage.py createsuperuser 

## Now open the browser and hit the server
-  http://127.0.0.1:8000/api/users/




Test DataBase setup for running test cases
==================================================
## Create a test database
-  CREATE DATABASE test_datapeace_db

## Grant privileges in database
-  GRANT ALL ON test_datapeace_db.* TO 'datapeace'@'localhost' IDENTIFIED BY 'datapeace@123

## Run the test case
-  python manage.pt test