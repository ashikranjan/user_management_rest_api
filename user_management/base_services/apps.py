from django.apps import AppConfig


class BaseServicesConfig(AppConfig):
    name = 'base_services'
