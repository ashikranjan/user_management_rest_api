from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

class PaginationService(object):
    def __init__(self, page_size=5, page_number=1):
        self.page_size = page_size
        self.page_number = page_number

    def get_paginated_data(self, data_list):
        ret = {}
        paginator = Paginator(data_list, self.page_size)
        try:
            data = paginator.page(self.page_number)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        ret['data'] = data
        ret['count'] = paginator.count
        return ret