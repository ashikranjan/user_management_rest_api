from rest_framework import status


class APIBaseService(object):
    def __init__(self):
        self.response = {
            "code": "",
            "data": {},
            "errors": {},
            "message": ""
        }

    @property
    def ret(self):
        return self.response
    
    def set_response(self, code=status.HTTP_200_OK, data={}, errors={}, message="Success"):
        self.response = {
            "code": code,
            "data": data,
            "errors": errors,
            "message": message
        }
        return self.response

    def record_created(self, code=status.HTTP_201_CREATED, data={}, errors={}, message="Created"):
        self.response = {
            "code": code,
            "data": data,
            "errors": errors,
            "message": message
        }
        return self.response

    def pre_condition_failed(self, code=status.HTTP_412_PRECONDITION_FAILED, data={}, errors={}, message="Pre Condition Failed"):
        self.response = {
            "code": code,
            "data": data,
            "errors": errors,
            "message": message
        }
        return self.response

    def get_baked_403_response(self):
        return 
        {
            "code": status.HTTP_403_FORBIDDEN,
            "data": {},
            "errors": ['Forbidden'],
            "message": "Forbidden"
        }

    def action_performed_no_content(self):
        return 
        {
            "code": status.HTTP_204_NO_CONTENT,
            "data": {},
            "errors": [],
            "message": "Success"
        }

    def get_baked_400_response(self, errors):
        return 
        {
            "code": status.HTTP_400_BAD_REQUEST,
            "data": {},
            "errors": errors,
            "message": "Bad Request"
        }

    def get_404_response(self, errors):
        return {
            "code": status.HTTP_404_NOT_FOUND,
            "data": {},
            "errors": errors,
            "message": "Bad Request"
        }

    def set_data(self, data):
        self.response["data"] = data
        return self.response

    def set_errors(self, errors):
        self.response["errors"] = errors
        return self.response

    def set_message(self, message):
        self.message = message

    def set_response_code(self, response_code):
        self.code = response_code
